package com.pawelbanasik;

public class Exercises {
	double[] array = { -5, 2.2, 3.011, -10, 15, -13, 123.14, -1 / 2.0 };

	public int[] zad1() {
		int[] array = new int[6];
		for (int i = 0; i <= 5; i++) {
			array[i] = i;

		}

		return array;
	}

	public int[] zad2() {

		int[] array = new int[6];
		// for (int i = 0; i <= 10; i++) {
		for (int i = 0; i <= 10; i = i + 2) {
			if (i % 2 == 0) {
				// array[i/2] = i;
				array[i / 2] = i;
			}

		}
		return array;
	}

	public int[] zad3() {

		int[] array = new int[11];

		for (int i = 100; i <= 130; i = i + 3) {
			array[(i - 100) / 3] = i;

		}
		return array;

	}

	public int[] zad3_2() {

		int[] array = new int[11];

		for (int i = 100, j = 0; i <= 130; i = i + 3, j++) {
			array[j] = i;

		}
		return array;

	}

	public int[] zad4() {

		int[] array = new int[10];
		int i = 0;
		while (i < 10) {

			array[i] = i;
			i++;
		}
		return array;
	}

	public int[] zad5() {
		int[] array = new int[5];
		int i = 0;
		do {
			array[i] = i;
			i++;
		} while (i < 5);

		return array;
	}

	public void zad8() {

		// bardzo wazny zapis -1/2 da zero w javie

		double sum = 0;

		for (int i = 0; i < array.length; i++) {

			if (array[i] < 0) {
				sum += array[i];
			}

		}
		System.out.println(sum);
	}

	public void zad9() {

		double sum = 0;
		// bez this mozna bo nie ma takiej samej nazwy
		for (int i = 0; i < array.length; i++) {

			if (i % 2 != 0) {

				sum += array[i];
			}
		}
		System.out.println("Suma liczb nieparzystych w tablicy wynosi: " + sum);
	}

	public void zad10() {

		double sum = 0;
		int i = 0;
		while (i < array.length) {

			if (array[i] > 0) {

				sum += array[i];

			}
			i++;
		}
		System.out.println(sum);
	}

	public void zad11() {

		double sum = 0;
		int i = 0;
		while (i < array.length) {

			if (array[i] % 1 == 0) {

				sum += array[i];

			}
			i++;
		}
		System.out.println("Suma liczb całkowitych tablicy wynosi: " + sum);
	}

	public void printAdresses(String street, int number) {
		String klatka = "A";
		//
		//
		for (int i = 1; i < 4; i = i + 2) {

			for (int j = 1; j <= 12; j++) {

				if (j >= 1 && j <= 6) {

					klatka = "A";
				} else if (j >= 7 && j <= 12) {
					klatka = "B";
				}
				System.out.println(street + i + klatka + "numer" + j);
			}
			// String cage ="A";
			// for (int i =1; i < number; i+= 2){
			// for(int j=1; j<= 12; j++) {
			//
			//
			// if(j > 6) {
			// cage = "B";
			// }
			// } else {
			// cage = "A";
			// }
			//
		}
	}
	// }

	public void printRangeNumbers(int a, int b) {

		for (int i = a; i <= b; i++) {
			if (i % 2 == 0) {

				System.out.print(i);
			}
		}

		for (int j = b; j >= a; j--) {
			if (j % 2 != 0) {

				System.out.print(j);
			}

		}
	}

	public void printArray(int[] array) {
		for (int i = 0; i < array.length; i++) {

			System.out.println("Index: " + i + ", Value: " + array[i]);
		}

	}

}
